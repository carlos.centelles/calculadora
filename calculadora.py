import sys

def multiplicar_numeros(num1, num2):
    resultado = num1 * num2
    return resultado

if __name__ == "__main__":
    # Verificar si se proporcionan los dos argumentos esperados
    if len(sys.argv) != 3:
        print("Has introducido demasiados numeros(introduce solo dos numeros)")
    else:
        # Convertir los argumentos de la línea de comandos a números
        try:
            numero1 = float(sys.argv[1])
            numero2 = float(sys.argv[2])
        except ValueError:
            print("Por favor, asegúrate de ingresar números válidos.")
            sys.exit(1)

        # Calcular el resultado y mostrarlo
        resultado = multiplicar_numeros(numero1, numero2)
        print(f"El resultado de multiplicar {numero1} y {numero2} es: {resultado}")

