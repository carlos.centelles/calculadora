import unittest
from unittest.mock import patch
from calculadora import multiplicar_numeros

class TestMultiplicarNumeros(unittest.TestCase):

    def test_multiplicar_numeros(self):
        # Prueba con números enteros
        resultado = multiplicar_numeros(2, 3)
        self.assertEqual(resultado, 6)

        # Prueba con números decimales
        resultado = multiplicar_numeros(2.5, 4)
        self.assertEqual(resultado, 10.0)

        # Prueba con un número negativo
        resultado = multiplicar_numeros(-3, 5)
        self.assertEqual(resultado, -15)

if __name__ == "__main__":
    unittest.main()
